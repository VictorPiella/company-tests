Input: ["baseball", "a,all,b,ball,bas,base,cat,code,d,e,quit,z"]
Output: base,ball
Input: ["abcgefd", "a,ab,abc,abcg,b,c,dog,e,efd,zzzz"]
Output: abcg,efd

function ArrayChallenge(strArr) { 
  var table =  {};
  var input = strArr[0];
  var subArray = strArr[1].split(',');

  for(i=0; i<subArray.length; i++){
      table[subArray[i]] = true;
  }

  for(i = 0 ; i < input.length; i++){
    
    var first = input.substring(0, i);
    var last = input.substring(i, input.length);

    if(table[first] && table[last]){
      return [first,last].join(',');
    }
  }
  // code goes here  
  return '';

}
   
// keep this function call here 
console.log(ArrayChallenge(readline()));