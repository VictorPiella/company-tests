
N = 4;
S = '1B 2C,2D 4D'
T = '2B 2D 3D 4D 4A'

function solution(N, S, T) {
    // write your code in JavaScript (Node.js 8.9.4)
    S= S.split(',');
    var ships = []; 
    for(var i= 0; i< S.length; i++){
    		var ship = {};
        var [pos1, pos2] = S[i].split(' ');
        var x1 = pos1.substring(0, (pos1.length-1));
        var x2 = pos2.substring(0, (pos2.length-1));
        var y1 = pos1[pos1.length - 1];
        var y2 = pos2[pos2.length - 1];
        for(y=y1.charCodeAt(0); y<=y2.charCodeAt(0); ++y){
            for(x=x1; x<=x2; ++x){
            	var key = x + String.fromCharCode(y)
            	ship[key]= false ;
        		}
        }
        ships[i] = ship;
    }
  
    T = T.split(' ');
    for(var i= 0; i< T.length; i++){
       for(var j = 0 ; j< ships.length; j++){
       		if(ships[j][T[i]] !== undefined){
          	ships[j][T[i]] = true;
          }
        }
    }
    
    var hittedSum = 0;
    var deadSum = 0;
    for (var i = 0; i < ships.length; ++i) {
      var hitted = false;
      var dead = true;
      
      var ship = ships[i];
      for (var key in ship) {
        if (ship[key]){
          hitted = true;
        } else {
          dead = false;
        }
      }
      if (dead) {
       ++deadSum;
      } else if(hitted) {
       ++hittedSum;
      }
    }
    return deadSum + "," + hittedSum;
}

console.log(solution(N, S, T));