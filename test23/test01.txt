Nathan has completed his very ?rst programming test and is now wondering about
his score. He received an email containing the ?nal report about the task he solved
in the test, but it omitted the score.
Nathan never gives up, so he decided to compute his result from the report. He
found out that:
. his program was tested on several test cases;
0 every test case has given one of the following results: "0K", "Wrong answer",
"Time limit exceeded" or "Runtime error";
0 test cases were organized into groups numbered by consecutive natural
numbers;
0 his program scored points for a group only when the result of every test case
in the group was "OK".
In the report, test cases are named according to the following rule: if the test case
is the only test case in the group, its name is [task name] + [group number]. If there
is more than one test in the group, its name is extended by a lowercase English
letter which is different for each test case. Example test case names are: testl,
testZa, test. In this example, test2a and test form one group and
testl forms another.
Nathan's score is the number of groups his program passed multiplied by 100 and
divided by the total number of groups. If the result is fractional, the number of H
points is rounded to the highest integer not greater than the result. For example, if
the program passed two groups out of three, the result is 2 * 100/ 3 = 66.(6), so
Nathan would gain 66 points.
Write a function
function solution(‘l‘, R);
that, given the names and results of all the test cases, returns the number of points
Nathan scored. T and R are arrays (containing strings) of length N. T[l] is a string
corresponding to the I-th test case name; R[l] is the result of this test case,
containing one of four possible values ("0K", "Wrong answer", "Time limit
exceeded"or"Runtime error")
For example, for data:
T[O] = "testla" R[O] = "Wrong answer"
T[l] = "testZ" R[l] = "OK"
T[Z] = "testlb" R[2] = "Runtime error"
T[ 3] = "testlc" R[ 3] = "OK"
T[4] = "test3" R[4] = "Time limit exceeded"
the function should return 33. Nathan's program passed only the second group.
For another set of data:
T[O] = "codilityl" R[O] = "Wrong answer"
T[ 1] = "codility3" R[ 1] = "OK"
T[Z] = "codilityZ" R[2] = "OK"
T[3] = "codility4b" R[3] = "Runtime error"
T[4] = "codility4a" R[4] = "OK"
he function should return 50. Nathan's program passed the second and third
groups.
ssume that:
o N is an integer within the range [1..300];
. arrays T and R have the same length;
0 every test case appears in table T exactly once;
. tests groups are numbered by consecutive natural numbers from 1;
0 tests cases in groups containing at least two tests are distinguished
by a lowercase letter suf?x in alphabetical order from a;
. every element in T is a valid test name;
0 the task name is identical for every test case, contains only English
lowercase letters and is of a length not greater than 30 letters;
0 each string in array R has one of the following values: "0K", "Wrong
answer","Time limit exceeded","Runtime error".
In your solution, focus on correctness. The performance of your solution will not be
he focus of the assessment