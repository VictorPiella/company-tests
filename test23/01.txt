var T  = ["test1a", "test2", "test1b", "test1c", "test3"]
var R = ["Wrong answer", "OK", "Runtime Error", "OK", "Time limit exceeded"]



function isNumeric(num){
  return !isNaN(num)
}

function solution(T, R) {
 var nGrups= 0;
 var correctGrups= 0;
 var dict = {};
 for(var i= 0; i< T.length; i++){
  var lastChar = T[i].slice(-1);
  if(isNumeric(lastChar)){
  	nGrups++;
  	correctGrups+=(R[i] === "OK");
  }else{
  	var result =( R[i] === "OK");
    var k = T[i].slice(0,-1);
    if(k in dict){
    	dict[k] &= result;
    }else{
    	dict[k] = result;
    }
  }
 }
 nGrups += Object.keys(dict).length;
 for(var key in dict){
 	correctGrups += dict[key];
 }

 return Math.floor((correctGrups * 100)/ nGrups);
}


console.log(solution(T, R));
