# Front-End Developer - Technical Assignment

## Simple Browser Based Game

This simple game server is intended to be used by Front End developers top complete their technical assessment. The server is their counterpart to the front end client app they have been asked to implement (please ensure you have read the main assignment first).


### Running the Server:

Using either method below, the server should become available on:

      http://localhost:8089

*Please ensure you have no other services running on 8089 on your machine*

#### Docker Build (Recommended)

With docker installed on your systems command line do the following:

##### Build

```
docker-compose build
```

##### Run

```
docker-compose up
```

#### Local Build

Without docker, you can build locally if golang 1.1+ is installed on your system

##### Run

```
go run ./cmd/sbbg
```

## Service Endpoints

The server runs on port **8089** and offers two endpoints:

### Join

Called by clients to join the current running server and take part in the next game.

#### POST Request with raw JSON as body

```js
  {
    name: "Your name",  // Display name for the player
    first: 3,           // First choice
    second: 5           // Second choice
  }
```

#### On Success:

```js
{
  status: 200,
  type: "Success",
  title: "Joined Game",
  detail: "Welcome to the game, player ;)"
}
```

#### On Failure (Example, for duplicate name)

```js
  {
    status: 400,
    type: "Error",
    title: "Invalid Request",
    detail: "Invalid name: There is already a player here with that name"
  }
```

### Subscribe

Calling the subscribe endpoint will establish a WebSocket

    `ws://localhost:8089/subscribe`

Sent data will be ignored by the server. The socket is intended to send events from the server to the client.

You will likely need to use a web socket library written for your associated frontend framework.

## Events

The web socket sends event objects to the client to keep it updated of game state changes. The generic event structure looks like this:

```js
 {
   type: "name",
   data: {}
 }
```

The section below describes each event server currently emits:

#### Player Registered

Triggered whenever a player registers to play the next game. Returns a `Game` object as its data

```js
  {
    type: "Player Registered",
    data: { // Game
      players: [{GamePlayer}], // Array of Game Players
      round: number,
      numbers: [number], // Array of number selected each round
      top_score: number,
      winner: {GamePlayer},
    }
  }
```



#### Player Joined

Triggered when a group of waiting players are added to the game, becoming a `GamePlayer`.

```js
  {
    type: "Player Joined",
    data:
    {
      [
        {  // GamePlayer
           name: "string",
           upper: number,
           lower: number,
           score: number,
           winner: bool
        },
      ]
    }
  }
```

#### Player Left

Triggered when a player leaves the game. Currently, only triggered when their WebSocket drops.

```js
  {
    type: "Player Left",
    data:
  }
```

#### Played Round

Event triggered whenever a round is played. Returns the round number and the leaderboard (list of game players in rank order)

```js
  {
    type: "Played Round",
    data: {
      leader_board: {
        [ {GamePlayer}, ]
      },
      round: number
    }
  }
```

#### Game Created

Not Implemented

```js
  {
    type: "Game Created",
    data:
  }
```

#### Game Started

Triggered when the countdown for a game start is completed. Should normally have an associated data number of zero, i.e. countdown to game start is complete

```js
  {
    type: "Game Started",
    data: number
  }
```

#### Game Completed

Triggered when a winning state is found. Returns the Game Player object that has won the game

```js
  {
    type: "Game Completed",
    data: { GamePlayer }
  }
```

#### Game Waiting

Triggered when the the engine ticks over and does not find enough players to start a game.

```js
  {
    type: "Game Waiting",
    data: null
  }
```

#### Countdown Started

Triggered when the game engine ticks and find enough players have joined the game to start a countdown to the game starting.

```js
  {
    type: "Countdown Started",
    data: number
  }
```

#### Counting Down

Triggered every tick the engine takes when in countdown mode, i.e. 5, 4, 3, 2, 1!

```js
  {
    type: "Counting Down",
    data: number
  }
```

#### Game Reset

Triggered when a game has completed and the cool down period has passed. Scores and leaderboard are reset. Current cool down is zero, i.e. Reset triggers directly behind a game complete event.

```js
  {
    type: "Game Reset",
    data: {Game}
  }
```
