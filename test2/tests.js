describe('Prime Numbers', () => {
  it('should do some demo tests', () => {
    //jasmine unit tests demo
    expect(true).toEqual(true);
    expect(5).toEqual(5);
    expect(5).not.toEqual(6);
    expect([1, 2, 3]).toEqual([1, 2, 3]);
    expect([1, 2, 3]).not.toBe([1, 2, 3]); //by reference
  });

  it('should return palindromic prime numbers', () => {
    expect(getPrimeNumbers()).toEqual([2, 3, 5, 7, 11, 101, 131, 151, 181, 191]);
  });
});
