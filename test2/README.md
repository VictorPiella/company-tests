# Palindromic Prime Numbers

> A Palindrome is a number which reads the same backward as forward such as `1, 2, 3, ... ,11, 22, ..., 99, 101,`

>  A Prime Number is a number that is divisible only by itself and `1`, e.g. `2, 3, 5, 7, 11, 13, 17, 19, ...`

**Todo**
1. Write a function which calculates Palindromic Prime Numbers
2. Display results in the browser
3. Support simple paging - only `Next` and `Previous` buttons required. Displayable results set is limited to `10` per page

**Reminders**
* Only use *Vanilla JS* (native JS), do not include external libraries
* Feel free to use *ES6*
* Feel free to add or change some unit tests (`tests.html` and `tests.js`)

---

We expect you to do your best, don't stress yourself, it's ok to skip some parts

**Good Luck!**
